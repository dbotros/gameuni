package com.davidbotros.gameUni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.davidbotros.gameUni.model.FieldType;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.service.FieldService;
import com.davidbotros.gameUni.service.GameService;


 @RestController
 @RequestMapping("/services/field")
public class FieldController {
	 
	 @Autowired 
	 public FieldService fieldService;
	 @Autowired
	 public GameService gameService;
	 
	 @RequestMapping(value= "/{type}/{visited}/{x}/{y}/{game_id}", method = RequestMethod.POST)
	 public void setField(@PathVariable("type")FieldType type,
			 @PathVariable("visited") Boolean visited,
			 @PathVariable("x") Integer x,
			 @PathVariable("y") Integer y,
			 @PathVariable("game_id") Game game_id){
		 fieldService.saveNewField(type, visited, x, y, game_id);
	 

	 }
 	 
}
