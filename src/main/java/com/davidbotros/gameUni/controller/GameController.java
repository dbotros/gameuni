package com.davidbotros.gameUni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.davidbotros.gameUni.model.Player;
import com.davidbotros.gameUni.service.GameService;
import com.davidbotros.gameUni.service.PlayerService;

@RestController
@RequestMapping("/services/game")
public class GameController {

	@Autowired
	private GameService gameService;
	@Autowired
	private PlayerService playerService;
	
	@RequestMapping(value="/{status}/{player_id}", method = RequestMethod.POST)
	public void createGame (@PathVariable("player_id") Player player_id){
		
		gameService.createGame(player_id);
		
	}
	
}


