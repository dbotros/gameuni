package com.davidbotros.gameUni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.model.PlayerType;
import com.davidbotros.gameUni.service.GameStatusService;


@RestController
@RequestMapping("/services/gamestatus")
public class GameStatusController {

	@Autowired
	private GameStatusService gameStatusService;
	
	@RequestMapping(value="/{playerType}/{game_id}/{gameStatus}/{tressureStatus}/{tressure_x}/{tressure_y}/{town_x}/{town_y}", method =RequestMethod.POST)
	public void announceStatus (@PathVariable("playerType")PlayerType playerType,
			                    @PathVariable("game_id")Game game_id,
			                    @PathVariable("gameStatus") Boolean game_Status,
			                    @PathVariable("tressureStatus") Boolean tressureStatus,
			                    @PathVariable("tressure_x") Integer tressure_X,
			                    @PathVariable("tressure_y") Integer tressure_Y,
			                    @PathVariable("town_x") Integer town_X,
			                    @PathVariable("town_y")Integer town_Y){
		gameStatusService.setStatus(playerType, game_id, game_Status, tressureStatus, tressure_X, tressure_Y, town_X, town_Y);


	}

}



	

