package com.davidbotros.gameUni.controller;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.davidbotros.gameUni.model.ActionType;
import com.davidbotros.gameUni.model.Field;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.model.Player;
import com.davidbotros.gameUni.model.PlayerType;
import com.davidbotros.gameUni.service.ActionService;


@RestController 
@RequestMapping("/services/actions") 
public class ActionController {
	
	
	
	@Autowired
	private ActionService actionService;
	
	
	

	@RequestMapping(value= "{movmentType}/{source}/{destination}/{scanAround}/{gameId}/{playerId}/{playerType}/{date}",method = RequestMethod.GET)
	public void creatAction(@PathVariable("movmentType") ActionType type,
		                  @PathVariable("source")com.davidbotros.gameUni.model.Field source,				  
		                  @PathVariable("destination") Field destination,
		                  @PathVariable("scanAround") Boolean scanAround,
		                  @PathVariable("gameId")Game game_id,
		 				  @PathVariable("playerId")Player player_id,
		 				  @PathVariable("playerType")PlayerType playerType,
                         @PathVariable("date")Date date){
			actionService.actionToField(type, source, destination, scanAround, game_id, player_id, playerType);
			
	}
	
	
}


