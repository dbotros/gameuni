package com.davidbotros.gameUni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.davidbotros.gameUni.model.BettingType;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.service.BettingService;
import com.davidbotros.gameUni.service.GameService;



 @RestController
 @RequestMapping("/services/betting")
 public class BettingController {
	 
	 
	 @Autowired 
	 public BettingService bettingService;
	 @Autowired 
	 public GameService gameService;
	 

	 
	 @RequestMapping(value= "/{name}/{amount}/{type}/{game_id}", method = RequestMethod.GET)
	 public void setBet(@PathVariable ("name")String name,
	 	@PathVariable ("amount")Integer amount,
	 	@PathVariable("type")BettingType type,
	 	@PathVariable ("game_id")Game game_id){
	 
	 bettingService.setBetting(name, amount, type, game_id);
	 
		 
	 }
 }
