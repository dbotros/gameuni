package com.davidbotros.gameUni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.davidbotros.gameUni.service.PlayerService;


@RestController
@RequestMapping("/services/player")
public class PlayerController {
	
	@Autowired
	private PlayerService playerService;
	
	
	@RequestMapping(value="/{firstName}/{lastName}/{email}", method = RequestMethod.GET)
	public void createPlayer(@PathVariable("firstName") String FirstName,
			@PathVariable("lastName")String lastName,
			@PathVariable("email")String email){
		
		playerService.saveNewPlayer(FirstName, lastName, email);
		
		
	}

}
