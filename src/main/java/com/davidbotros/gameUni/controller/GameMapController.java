package com.davidbotros.gameUni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.davidbotros.gameUni.model.FieldType;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.model.Player;
import com.davidbotros.gameUni.model.PlayerType;
import com.davidbotros.gameUni.service.GameMapService;

 @RestController 
 @RequestMapping("/services/gamemap")
 public class GameMapController {
	
	 @Autowired
	 private GameMapService gameMapService;
	 
	 @RequestMapping (value= "/{xSize}/{ySize}/{game_id}/{playerType}/{fieldType}/{player_id}", method = RequestMethod.GET)
	public void createMap(@PathVariable("xSize")Integer xSize,
			              @PathVariable("ySize") Integer ySize,
			              @PathVariable("game_id")Game game_id,
			              @PathVariable("playerType")PlayerType playerType,
			              @PathVariable("fieldType")FieldType fieldType,
			              @PathVariable("player_id")Player player_id){
		 
	
		 gameMapService.setMap(xSize, ySize, game_id, playerType, fieldType, player_id);
	 }
	

}
