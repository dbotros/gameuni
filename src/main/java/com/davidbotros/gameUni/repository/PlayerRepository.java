package com.davidbotros.gameUni.repository;

import org.springframework.data.repository.CrudRepository;

import com.davidbotros.gameUni.model.Player;

public interface PlayerRepository extends CrudRepository< Player, Long>  {

}
