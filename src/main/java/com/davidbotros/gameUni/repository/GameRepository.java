package com.davidbotros.gameUni.repository;

import org.springframework.data.repository.CrudRepository;

import com.davidbotros.gameUni.model.Game;

public interface GameRepository extends CrudRepository<Game, Long> {

}
