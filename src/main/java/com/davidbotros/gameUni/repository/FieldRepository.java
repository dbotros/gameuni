package com.davidbotros.gameUni.repository;

import org.springframework.data.repository.CrudRepository;

import com.davidbotros.gameUni.model.Field;

public interface FieldRepository extends CrudRepository<Field, Long> {

}
