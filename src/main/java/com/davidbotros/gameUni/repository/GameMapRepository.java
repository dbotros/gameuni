package com.davidbotros.gameUni.repository;

import org.springframework.data.repository.CrudRepository;


import com.davidbotros.gameUni.model.GameMap;

public interface GameMapRepository extends CrudRepository<GameMap, Long> {

}
