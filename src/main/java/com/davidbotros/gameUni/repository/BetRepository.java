package com.davidbotros.gameUni.repository;

import org.springframework.data.repository.CrudRepository;

import com.davidbotros.gameUni.model.Betting;

public interface BetRepository extends CrudRepository< Betting, Long>  {

}
