package com.davidbotros.gameUni.repository;

import org.springframework.data.repository.CrudRepository;

import com.davidbotros.gameUni.model.GameStatus;

public interface GameStatusRepository extends CrudRepository<GameStatus, Long> {

}
