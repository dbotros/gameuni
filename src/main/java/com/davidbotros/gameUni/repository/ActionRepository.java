package com.davidbotros.gameUni.repository;

import org.springframework.data.repository.CrudRepository;

import com.davidbotros.gameUni.model.Action;

public interface ActionRepository extends CrudRepository<Action, Long> {

}
