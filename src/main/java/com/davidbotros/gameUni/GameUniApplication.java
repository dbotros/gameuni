package com.davidbotros.gameUni;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class GameUniApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(GameUniApplication.class);

	public static void main(String[] args) {
		
		
		
		SpringApplication.run(GameUniApplication.class, args);
		
		LOGGER.info("Logging is working normally. Pattern configuration in application.properties");
		
		
		
		
	}	

}
