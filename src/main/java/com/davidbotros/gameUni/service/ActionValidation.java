package com.davidbotros.gameUni.service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.davidbotros.gameUni.model.Action;
import com.davidbotros.gameUni.model.Field;
import com.davidbotros.gameUni.model.FieldType;
import com.davidbotros.gameUni.model.GameStatus;
import com.davidbotros.gameUni.model.PlayerType;
import com.davidbotros.gameUni.repository.ActionRepository;


public class ActionValidation {
	
	int player_one_counter=0;
	int player_two_counter=0;
	boolean status;
	@Autowired 
	private GameStatus gameStatus;
	@Autowired 
	private GameMapService gameMapService;
	@Autowired
	private FieldService fieldService;
	@Autowired
	private GameService gameService;
	@Autowired
	private ActionRepository repository;
	Logger logger = LoggerFactory.getLogger(ActionValidation.class);
	
	public boolean ifInMap(Field destination){
		  
		if (destination.getX() < 0 || destination.getX() > 7) {
			 logger.error("Player went out of the map");
			 status= false;
			 gameStatus.setGameStatus(status);
			 }
		 if (destination.getY() < 0 || destination.getY() > 3) {
			 logger.error("Player went out of the map");
			 status= false;
			 gameStatus.setGameStatus(status);
			 }
		 if(destination.getType() == FieldType.WATER){	
				logger.error("Avatar can't go in water");
				 status= false;
				 gameStatus.setGameStatus(status);
			 }
		return status;
	 }


	public Action ifMountain(PlayerType playerType, Field destination, Action action) {
		
		if(destination.getType() == FieldType.MOUNTAIN){	
			if(playerType== PlayerType.FIRST_PLAYER){
				if (player_one_counter==4||player_one_counter==8||player_one_counter==12||player_one_counter==16){
					repository.save(action);
				}
				else player_one_counter++;
			}
			if(playerType== PlayerType.SECOND_PLAYER){
				if (player_two_counter==4||player_two_counter==8||player_two_counter==12||player_two_counter==16){
					repository.save(action);
				}
				else player_two_counter++;
			}
		}
		return (Action) repository;
	}
	

	public boolean ifMountainWasCorrect(PlayerType playerType, Field source) {
		if(source.getType() == FieldType.MOUNTAIN){
			if(playerType== PlayerType.FIRST_PLAYER){
				if (player_one_counter!=4||player_one_counter!=8||player_one_counter!=12||player_one_counter!=16){
					logger.error("Invalid Move by First Player");
					 status= false;
					 gameStatus.setGameStatus(status);
				}
			}
			if(playerType== PlayerType.SECOND_PLAYER){
				if (player_two_counter!=4||player_two_counter!=8||player_two_counter!=12||player_two_counter!=16){
					logger.error("Invalid Move by Second Player");
					 status= false;
					 gameStatus.setGameStatus(status);
					}
			}
		}
		return status;
	}
	public Action ifGrass(Field destination, Action action){
		if (destination.getType()== FieldType.GRASS){
			repository.save(action);
		}return (Action) repository;
	}
	public Action ifTressure(Field destination, PlayerType playerType){
		if(destination.getType() == FieldType.TRESSURE && playerType == PlayerType.FIRST_PLAYER){
			logger.info("Player One found the tressure");
		}
		if(destination.getType() == FieldType.TRESSURE && playerType == PlayerType.FIRST_PLAYER){
			logger.info("Player Two found the tressure");
		}return (Action)logger;
	}
	
}	
				
			
		
	
	