package com.davidbotros.gameUni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.model.GameStatus;
import com.davidbotros.gameUni.model.PlayerType;
import com.davidbotros.gameUni.repository.GameStatusRepository;

@Service
public class GameStatusService {
	
	@Autowired
	private GameStatusRepository repository;
	
	public GameStatus setStatus(PlayerType playerType, Game game_id, boolean game_Status, boolean tressureStatus,Integer tressure_X, Integer tressure_Y,Integer town_X,Integer town_Y ){
	
		GameStatus gameStatus = new GameStatus();
		gameStatus.setPlayerType(playerType);
		gameStatus.setGame_id(game_id);
		gameStatus.setGameStatus(game_Status);
		gameStatus.setTressureStatus(tressureStatus);
		gameStatus.setTressure_X(tressure_X);
		gameStatus.setTressure_Y(tressure_Y);
		gameStatus.setTown_X(town_X);
		gameStatus.setTown_Y(town_Y);
		return repository.save(gameStatus);
		
	}

}

