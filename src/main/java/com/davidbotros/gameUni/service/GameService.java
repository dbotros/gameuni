package com.davidbotros.gameUni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.model.Player;
import com.davidbotros.gameUni.repository.GameRepository;

@Service
public class GameService {
	
	@Autowired
	private GameRepository repository;
	
	@Autowired 
	private PlayerService playerService;
	
	public Game createGame(Player player){
		
		
		
		Game game = new Game();
		return repository.save(game);
		
	}
}
	
