package com.davidbotros.gameUni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.davidbotros.gameUni.model.FieldType;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.model.GameMap;
import com.davidbotros.gameUni.model.Player;
import com.davidbotros.gameUni.model.PlayerType;
import com.davidbotros.gameUni.repository.GameMapRepository;

@Service
public class GameMapService {
	
	private MapValidation mapValidation= new MapValidation();
	@Autowired
	private GameMapRepository repository;
	@Autowired 
	private GameService gameService;
	@Autowired 
	private PlayerService playerService;
	
	public  GameMap setMap(Integer xSize, Integer ySize, Game id,PlayerType playerType, FieldType fieldType,Player player_id){
		
		GameMap map = new GameMap();
		map.setxSize(xSize);
		map.setySize(ySize);
		map.setGame_id(id);
		map.setPlayerType(playerType);
		map.setFieldType(fieldType);
		map.setPlayer(player_id);
		mapValidation.count(fieldType, playerType);
		mapValidation.checkCount(id,playerType);
		mapValidation.countBorders(ySize, fieldType, playerType);
		mapValidation.checkBorders(playerType,id);
		return map;
		
	}
	
}
