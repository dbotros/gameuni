package com.davidbotros.gameUni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;





import com.davidbotros.gameUni.model.Field;
import com.davidbotros.gameUni.model.FieldType;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.repository.FieldRepository;

@Service
public class FieldService {

	@Autowired
	private  FieldRepository repository;


	

	public  Field saveNewField(FieldType type, boolean visited, Integer x, Integer y,Game game){
		
		Field field = new Field();
		field.setType(type);
		field.setVisited(visited);
		field.setX(x);
		field.setY(y);
		field.setGame(game);
		return repository.save(field);
		
	}
	
}
