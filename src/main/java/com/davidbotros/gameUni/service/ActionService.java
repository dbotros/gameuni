package com.davidbotros.gameUni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.davidbotros.gameUni.model.Action;
import com.davidbotros.gameUni.model.ActionType;
import com.davidbotros.gameUni.model.Field;
import com.davidbotros.gameUni.model.FieldType;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.model.Player;
import com.davidbotros.gameUni.model.PlayerType;
import com.davidbotros.gameUni.repository.ActionRepository;
import com.davidbotros.gameUni.service.ActionValidation;

@Service
public class ActionService {
	private ActionValidation actionValidation= new ActionValidation();
	@Autowired
	private ActionRepository repository;
	
	@Autowired
	private FieldService fieldService;
	
	@Autowired 
	private GameService gameService;
	
	@Autowired 
	private GameMapService gameMapService;

	public Action actionToField(ActionType ActionType, Field source, Field destination, boolean scanAround, Game game_id, Player player_id,PlayerType playerType) {

		Action action = new Action();
		action.setType(ActionType);
		action.setSource(source);
		action.setDestination(destination);
		action.setGame(game_id);
		action.setPlayer(player_id);
		action.setPlayerType(playerType);
		actionValidation.ifInMap(destination);
		actionValidation.ifMountain(playerType, destination, action);
		actionValidation.ifMountainWasCorrect(playerType, source);
		actionValidation.ifGrass(destination, action);
		actionValidation.ifTressure(destination, playerType);
		
		return null;
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//		if (isInMap(destination)) {
//			discoverDestinationIfUnknown(destination);
//		}

//		if (scanAround) {
//			// Call gameServer for field type
//			//gameServerService.scanAround(destination)
//		}

	}

	private Field discoverDestinationIfUnknown(Field destination) {
		// Contact game Server for field type
		
		if(destination.getType() == FieldType.UNKNOWN){			
			//destination = gameServerService.getTypeForField(destination)
		}
		
		return destination;
		
	}

	private boolean isInMap(Field destination) {
		if (destination.getX() < 0 || destination.getX() > 7) {
			return false;
		}

		if (destination.getY() < 0 || destination.getY() > 3) {
			return false;
		}
		
		return true;
		
	}

}
