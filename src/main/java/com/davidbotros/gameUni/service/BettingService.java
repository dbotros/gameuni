package com.davidbotros.gameUni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.davidbotros.gameUni.model.Betting;
import com.davidbotros.gameUni.model.BettingType;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.repository.BetRepository;


@Service
public class BettingService {
	
	@Autowired
	private BetRepository repository;

	
	
	
	public Betting setBetting(String name, Integer amount, BettingType type, Game id){
		
		Betting betting = new Betting();
		betting.setAmount(amount);
		betting.setName(name);
		betting.setSettingOn(type);
		betting.setGame(id);
		return repository.save(betting);
	}
	
	
	

}
