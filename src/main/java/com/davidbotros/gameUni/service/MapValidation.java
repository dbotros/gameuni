package com.davidbotros.gameUni.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.davidbotros.gameUni.model.FieldType;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.model.GameStatus;
import com.davidbotros.gameUni.model.PlayerType;
import com.davidbotros.gameUni.repository.GameStatusRepository;


public class MapValidation {
	

	Logger logger = LoggerFactory.getLogger(MapValidation.class);
	int grass_one=0;
	int mountain_one=0;
	int water_one=0;
	int town_one=0;
	int grass_two=0;
	int water_two=0;
	int mountain_two=0;
	int town_two=0;
	boolean status;
	int borders_one=0;
	int borders_two=0;
	@Autowired
	private GameMapService gameMapService;
	@Autowired
	private GameService gameService;
	@Autowired
	private GameStatus gameStatus;
	@Autowired
	private GameStatusService gameStatusService;
	@Autowired
	private GameStatusRepository repository;

	
	

	
	
	public void count(FieldType field,PlayerType player){
		if (player == PlayerType.FIRST_PLAYER && field == FieldType.GRASS) {
			 grass_one++;
		 }
		if (player == PlayerType.FIRST_PLAYER && field == FieldType.WATER) {
			water_one++;
		}
		if (player == PlayerType.FIRST_PLAYER && field == FieldType.MOUNTAIN) {
			mountain_one++;
		}
		if (player == PlayerType.FIRST_PLAYER && field == FieldType.TOWN) {
			town_one++;
		}
		if (player == PlayerType.SECOND_PLAYER && field == FieldType.GRASS) {
			 grass_two++;
		 }
		if (player == PlayerType.SECOND_PLAYER && field == FieldType.WATER) {
			water_two++;
		}
		if (player == PlayerType.SECOND_PLAYER && field == FieldType.MOUNTAIN) {
			mountain_two++;
		}
		if (player == PlayerType.SECOND_PLAYER && field == FieldType.TOWN) {
			town_two++;
	}	
}

	public GameStatus checkCount(Game game_id, PlayerType playerType){
		if(grass_one<5 || grass_one>25){
			logger.error("Player one grass fields are not within limits ");
			status=false;
			 gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
			}
		if(mountain_one<3 || mountain_one>21){
			logger.error("Player one mounatins fields are not within limits ");
			status= false;
			 gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
			}
		if(water_one<4 || water_one>24){
			logger.error("Player one water fields are not within limits ");
			status= false;
			 gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
			}
		if(grass_two<5 || grass_two>25){
			logger.error("Player two grass fields are not within limits ");
			status= false;
			 gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
			}
		if(mountain_two<3 || mountain_two>21){
			logger.error("Player two mounatins fields are not within limits ");
			status= false;
			 gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
			}
		if(water_two<4 || water_two>24){
			logger.error("Player two water fields are not within limits ");
			status= false;
			 gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
			}
		if(town_one!=1){
			logger.error("Player one Castle was not set");
			status=false;
			 gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
		}
		if(town_two!=1){
			logger.error("Player Two Castle was not set");
			status=false;
			 gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
		}
		
		return repository.save(gameStatus);
	}
	public MapValidation countBorders(Integer ySize, FieldType field,PlayerType player){
		if(ySize==3 && field== FieldType.WATER && player == PlayerType.FIRST_PLAYER){
			borders_one++;
		}
		if(ySize==3 && field== FieldType.WATER && player == PlayerType.SECOND_PLAYER){
			borders_two++;
		}
		return null;
	}
	public GameStatus checkBorders(PlayerType playerType,Game game_id){
		if(borders_one > 3){
			logger.error("Water fields by border was exceeded by First player");
			status= false;
			gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
			}
		if(borders_two>3){
			logger.error("Water fields by borders was exceeded by Second Player");
			status= false;
			gameStatusService.setStatus(playerType, game_id, status, status, null, null, null, null);
			}
		return repository.save(gameStatus);
	}



	 
}
