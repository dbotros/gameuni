package com.davidbotros.gameUni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.davidbotros.gameUni.model.Player;
import com.davidbotros.gameUni.repository.PlayerRepository;

@Service
public class PlayerService {
	
	@Autowired
	private PlayerRepository repository;
	
	
	public  Player saveNewPlayer(String fname, String sname, String email){
		
		Player player = new Player();
		player.setFname(fname);
		player.setSname(sname);
		player.setEmail(email);
		return repository.save(player);
	}

}

