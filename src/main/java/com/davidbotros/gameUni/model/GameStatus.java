package com.davidbotros.gameUni.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
public class GameStatus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Enumerated(EnumType.STRING)
	private PlayerType playerType;
	@OneToOne
	private Game game_id;
	private boolean gameStatus;
	private Boolean tressureStatus;
	private Integer tressure_X;
	private Integer tressure_Y;
	private Integer town_X;
	private Integer town_Y;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public PlayerType getPlayerType() {
		return playerType;
	}
	public void setPlayerType(PlayerType playerType) {
		this.playerType = playerType;
	}
	public Game getGame_id() {
		return game_id;
	}
	public void setGame_id(Game game_id) {
		this.game_id = game_id;
	}
	public Boolean getGameStatus() {
		return gameStatus;
	}
	public void setGameStatus(boolean status) {
		this.gameStatus = status;
	}
	public Boolean getTressureStatus() {
		return tressureStatus;
	}
	public void setTressureStatus(Boolean tressureStatus) {
		this.tressureStatus = tressureStatus;
	}
	public Integer getTressure_X() {
		return tressure_X;
	}
	public void setTressure_X(Integer tressure_X) {
		this.tressure_X = tressure_X;
	}
	public Integer getTressure_Y() {
		return tressure_Y;
	}
	public void setTressure_Y(Integer tressure_Y) {
		this.tressure_Y = tressure_Y;
	}
	public Integer getTown_X() {
		return town_X;
	}
	public void setTown_X(Integer town_X) {
		this.town_X = town_X;
	}
	public Integer getTown_Y() {
		return town_Y;
	}
	public void setTown_Y(Integer town_Y) {
		this.town_Y = town_Y;
	}
	
	
	

}
