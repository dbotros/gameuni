package com.davidbotros.gameUni.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class GameMap {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Integer xSize;
	private Integer ySize;
	@OneToOne
	private Game game;
	@Enumerated(EnumType.STRING)
	private PlayerType playerType;
	@Enumerated(EnumType.STRING)
	private FieldType fieldType;
	@OneToOne
	private Player player;
	
	
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public FieldType getFieldType() {
		return fieldType;
	}

	public void setFieldType(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	public PlayerType getPlayerType() {
		return playerType;
	}

	public void setPlayerType(PlayerType playerType) {
		this.playerType = playerType;
	}

	public Game getGame_id() {
		return game;
	}

	public void setGame_id(Game game_id) {
		this.game = game_id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getxSize() {
		return xSize;
	}

	public void setxSize(Integer xSize) {
		this.xSize = xSize;
	}

	public Integer getySize() {
		return ySize;
	}

	public void setySize(Integer ySize) {
		this.ySize = ySize;
	}

}
