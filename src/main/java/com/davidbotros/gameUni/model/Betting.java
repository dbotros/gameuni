package com.davidbotros.gameUni.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Betting {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Enumerated(EnumType.STRING)
	private BettingType setOn;
	private Integer amount;
	private String name;
	@OneToOne
	private Game game;
	
	public BettingType getSetOn() {
		return setOn;
	}
	public void setSetOn(BettingType setOn) {
		this.setOn = setOn;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BettingType getSettingOn() {
		return setOn;
	}
	public void setSettingOn(BettingType setOn) {
		this.setOn = setOn;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
