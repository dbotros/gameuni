package com.davidbotros.gameUni.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Action {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Enumerated(EnumType.STRING)
	private ActionType type;
	@OneToOne
	private Field source;
	@OneToOne
	private Field destination;
	@OneToOne
	private Game game;
	@OneToOne
	private Player player;
	@Enumerated(EnumType.STRING)
	private PlayerType playerType;
	private Date lastModified = new Date();
	
	
	public PlayerType getPlayerType() {
		return playerType;
	}

	public void setPlayerType(PlayerType playerType) {
		this.playerType = playerType;
	}
	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ActionType getType() {
		return type;
	}

	public void setType(ActionType type) {
		this.type = type;
	}

	public Field getSource() {
		return source;
	}

	public void setSource(Field source) {
		this.source = source;
	}

	public Field getDestination() {
		return destination;
	}

	public void setDestination(Field destination) {
		this.destination = destination;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	
	
}
