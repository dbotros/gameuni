package com.davidbotros.gameUni;

import org.junit.Test; 
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import com.davidbotros.gameUni.model.Action;
import com.davidbotros.gameUni.model.ActionType;
import com.davidbotros.gameUni.model.Betting;
import com.davidbotros.gameUni.model.BettingType;
import com.davidbotros.gameUni.model.Field;
import com.davidbotros.gameUni.model.FieldType;
import com.davidbotros.gameUni.model.Game;
import com.davidbotros.gameUni.model.GameMap;
import com.davidbotros.gameUni.model.GameStatus;
import com.davidbotros.gameUni.model.Player;
import com.davidbotros.gameUni.model.PlayerType;
import com.davidbotros.gameUni.service.ActionService;
import com.davidbotros.gameUni.service.ActionValidation;
import com.davidbotros.gameUni.service.BettingService;
import com.davidbotros.gameUni.service.FieldService;
import com.davidbotros.gameUni.service.GameMapService;
import com.davidbotros.gameUni.service.GameService;
import com.davidbotros.gameUni.service.GameStatusService;
import com.davidbotros.gameUni.service.MapValidation;
import com.davidbotros.gameUni.service.PlayerService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameUniApplicationTests {

	@Autowired
	private ActionService actionService;
	@Autowired
	private FieldService fieldService;
	@Autowired
	private PlayerService playerService;
	@Autowired 
	private BettingService bettingService;
	@Autowired
	private GameService gameService;
	@Autowired
	private GameMapService gameMapService;
	@Autowired
	private GameStatusService gameStatusService;
	
	@Test
	public void EnteringData() {
		
		Player player = playerService.saveNewPlayer("maja", "yoanna", "beans@bebe");
		Player player2 = playerService.saveNewPlayer("Michael", "Wurst", "random@random");
		Game game = gameService.createGame(player);
		Game game2 = gameService.createGame(player2);
		Field field = fieldService.saveNewField(FieldType.MOUNTAIN, true, 3, 1, game);
		Field field2= fieldService.saveNewField(FieldType.GRASS, true, 3, 2, game);
		Field field3 = fieldService.saveNewField(FieldType.GRASS, true, 3, 3, game);
		Betting betting2 = bettingService.setBetting("MArtina", 20, BettingType.FIRST_PLAYER, game2);
		Betting betting = bettingService.setBetting("Micahel", 100, BettingType.NONE, game);
		GameStatus gameStatus2= gameStatusService.setStatus(PlayerType.SECOND_PLAYER, game, false, false, 2, 0, 3, 1);
		GameStatus gameStatus= gameStatusService.setStatus(PlayerType.FIRST_PLAYER, game, true, true, 2, 3, 4, 1);
		Action action = actionService.actionToField(ActionType.MOVE, field, field2, true, game, player, PlayerType.FIRST_PLAYER);
		Action action2 = actionService.actionToField(ActionType.MOVE, field2, field3, true, game, player2, PlayerType.SECOND_PLAYER);
		GameMap gameMap = gameMapService.setMap(3, 2, game, PlayerType.FIRST_PLAYER, FieldType.GRASS, player);
		GameMap gameMap2 = gameMapService.setMap(4, 1, game, PlayerType.SECOND_PLAYER, FieldType.WATER, player2);
		
		
		
	
		

	
		
		
		
	}

}
